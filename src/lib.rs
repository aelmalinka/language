//! Random Language Thing
// 2023-11-21 MLT TODO:
#![warn(
    clippy::pedantic,
    clippy::nursery,
    missing_docs,
    missing_debug_implementations
)]
#![deny(unsafe_op_in_unsafe_fn)]

pub mod ast;
pub mod frontend;

pub use frontend::parse;
