//! The tokens
use logos::{Lexer as LogosLexer, Logos, SpannedIter};
use snafu::prelude::*;

use super::parser::{Error as ParserError, TokenSnafu};
use crate::ast::Primitive;

/// Error tokenizing/parsing
#[derive(Clone, Debug, Default, PartialEq, Eq, Snafu)]
pub enum Error {
    /// An unspecified tokenization error
    #[default]
    Tokenization,
    /// Missing character in char literal
    CharError,
    /// An invalid integer literal
    IntError {
        #[doc(hidden)]
        source: std::num::ParseIntError,
    },
    /// An invalid float literal
    FloatError {
        #[doc(hidden)]
        source: std::num::ParseFloatError,
    },
}

/// All tokens for lexer
// 2023-09-03 MLT TODO: Copy
#[derive(Clone, Debug, PartialEq, Logos)]
// 2023-11-23 MLT TODO: ?
#[allow(missing_docs)]
// 2023-09-02 MLT TODO: full whitespace def
// 2023-11-23 MLT TODO: is this what we want?
#[logos(skip r"[ \t\r\n\f]+")]
#[logos(error = Error)]
pub enum Token<'a> {
    #[token(":")]
    Colon,
    #[token(",")]
    Comma,
    #[token(";")]
    SemiColon,
    #[token(".")]
    Dot,
    #[token("=")]
    Equal,
    #[token("+")]
    Plus,
    #[token("-")]
    Minus,
    #[token("*")]
    KleeneStar,
    #[token("/")]
    Slash,
    #[token("%")]
    Percent,
    #[token("&")]
    Ampersand,
    #[token("::")]
    Scope,
    #[token("{")]
    OpenBrace,
    #[token("}")]
    CloseBrace,
    #[token("(")]
    OpenParen,
    #[token(")")]
    CloseParen,
    #[token("[")]
    OpenBracket,
    #[token("]")]
    CloseBracket,
    #[token("->")]
    Return,

    #[token("fn")]
    Function,
    #[token("struct")]
    Struct,
    #[token("let")]
    Let,
    #[token("usize", |_| Primitive::Usize)]
    #[token("isize", |_| Primitive::Isize)]
    #[token("u64", |_| Primitive::U64)]
    #[token("i64", |_| Primitive::I64)]
    #[token("u32", |_| Primitive::U32)]
    #[token("i32", |_| Primitive::I32)]
    #[token("u8", |_| Primitive::U8)]
    #[token("f64", |_| Primitive::F64)]
    #[token("str", |_| Primitive::Str)]
    #[token("bool", |_| Primitive::Bool)]
    Primitive(Primitive),

    #[regex(r"[_a-zA-Z][_a-zA-Z0-9]*", |i| i.slice())]
    Identifier(&'a str),
    #[regex(r#""[^"]*""#, |i| &i.slice()[1..i.slice().len()-1])]
    LiteralString(&'a str),
    // 2023-09-02 MLT TODO: seperators
    // 2023-09-02 MLT TODO: default sizes
    // 2023-09-02 MLT TODO: specified sizes
    // 2023-11-23 MLT TODO: errors
    #[regex(r"-?(?:0|[1-9]\d*)(?:[eE][+-]?\d+)?", int)]
    LiteralInteger(i128),
    #[regex(r"-?(?:0|[1-9]\d*)(?:\.\d+)(?:[eE][+-]?\d+)?", float)]
    LiteralFloat(f64),
    #[regex(r"'.'", char)]
    LiteralChar(char),
    #[token("true", |_| true)]
    #[token("false", |_| false)]
    LiteralBool(bool),
}

fn int<'a>(i: &LogosLexer<'a, Token<'a>>) -> Result<i128, Error> {
    i.slice().parse::<i128>().context(IntSnafu)
}

fn float<'a>(i: &LogosLexer<'a, Token<'a>>) -> Result<f64, Error> {
    i.slice().parse::<f64>().context(FloatSnafu)
}

fn char<'a>(i: &LogosLexer<'a, Token<'a>>) -> Result<char, Error> {
    i.slice().chars().nth(1).context(CharSnafu)
}

/// The required type for lalrpop
pub type Spanned<T, L, E> = Result<(L, T, L), E>;

/// A wrapper around [`logos::SpannedIter`]
#[allow(missing_debug_implementations)]
pub struct Lexer<'a> {
    token_stream: SpannedIter<'a, Token<'a>>,
}

impl<'a> Lexer<'a> {
    /// Create [`Self`] from a str reference
    #[must_use]
    pub fn new(input: &'a <Token<'a> as Logos<'a>>::Source) -> Self {
        Self {
            token_stream: Token::lexer(input).spanned(),
        }
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = Spanned<Token<'a>, usize, ParserError>;

    fn next(&mut self) -> Option<Self::Item> {
        self.token_stream
            .next()
            .map(|(token, span)| Ok((span.start, token.context(TokenSnafu)?, span.end)))
    }
}
