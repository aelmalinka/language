//! The abstract syntax tree

/// The top-level AST
#[derive(Clone, Debug, PartialEq)]
pub struct Module<'a>(
    /// A Module is Items
    pub Vec<Item<'a>>,
);

/// Top level items
#[derive(Clone, Debug, PartialEq)]
pub enum Item<'a> {
    // 2023-09-02 MLT TODO: add Const
    /// A function definition
    Function {
        /// The name of the function
        name: Identifier<'a>,
        /// The parameters of the function
        params: Vec<(Identifier<'a>, Type<'a>)>,
        /// The return type
        r#return: Option<Type<'a>>,
        /// The block
        block: Block<'a>,
    },
    /// A Structure
    Struct {
        /// The name of the structure
        name: Identifier<'a>,
        /// The data of the structure
        fields: Vec<(Identifier<'a>, Type<'a>)>,
    },
    // 2023-09-02 MLT TODO: newtype?
    // 2023-09-02 MLT TODO: module
    // 2023-09-02 MLT TODO: trait?
}

/// An identifier
pub type Identifier<'a> = &'a str;

/// A qualified identifier
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct ScopedIdentifier<'a>(pub Vec<Identifier<'a>>, pub bool);

/// A program block
#[derive(Clone, Debug, PartialEq)]
pub struct Block<'a>(pub Vec<Statement<'a>>, pub Option<Expression<'a>>);

/// Statements
#[derive(Clone, Debug, PartialEq)]
pub enum Statement<'a> {
    /// An empty statement
    Empty,
    /// A new local binding
    // 2023-09-03 MLT TODO: pattern matching
    Binding(Identifier<'a>, Option<Type<'a>>, Expression<'a>),
    /// Assignment
    Assign(Identifier<'a>, Assignment, Expression<'a>),
    /// Expression
    Expression(Expression<'a>),
}

/// An expression
#[derive(Clone, Debug, PartialEq)]
pub enum Expression<'a> {
    /// A literal
    Literal(Literal<'a>),
    /// Most likely a local variable (todo)
    Value(Identifier<'a>),
    /// An unary operator
    Unary(Unary, Box<Expression<'a>>),
    /// An binary operator
    Binary(Box<Expression<'a>>, Binary, Box<Expression<'a>>),
    /// Struct constructor
    Struct(
        ScopedIdentifier<'a>,
        Vec<(Identifier<'a>, Option<Expression<'a>>)>,
    ),
    /// Function call
    FnCall(Box<ScopedIdentifier<'a>>, Vec<Expression<'a>>),
    /// Method call
    MethodCall(
        Box<ScopedIdentifier<'a>>,
        Identifier<'a>,
        Vec<Expression<'a>>,
    ),
    // 2023-09-02 MLT TODO: if's and the like
}

/// A unary operator
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Unary {
    /// Negate
    Neg,
}

/// A binary operator
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Binary {
    /// Add
    Add,
    /// Subtract
    Sub,
    /// Multiply
    Mul,
    /// Divide
    Div,
    /// Remainder
    Rem,
}

/// Assignment operations
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Assignment {
    /// Equals
    Eq,
}

/// A type
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Type<'a> {
    /// An idefined type
    Ident(ScopedIdentifier<'a>),
    /// A built-in type
    Primitive(Primitive),
    /// A reference (mut?)
    Reference(Box<Type<'a>>),
    /// `[T]`
    Slice(Box<Type<'a>>),
    /// `[T; _]`
    Array(Box<Type<'a>>, usize),
    /// Tuple
    Tuple(Vec<Type<'a>>),
}

/// The literals
// 2023-09-02 MLT TODO: specific sizes?
#[derive(Clone, Debug, PartialEq)]
pub enum Literal<'a> {
    /// Integer of undetermined size
    Integer(i128),
    /// Floating point of undetermined size
    Float(f64),
    /// String
    String(&'a str),
    /// Character
    Char(char),
    /// Boolean
    Bool(bool),
    /// Tuple
    Tuple(Vec<Literal<'a>>),
    /// Slice
    Slice(Vec<Literal<'a>>),
}

// 2023-09-02 MLT TODO: rest of types?
/// Built-in types
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Primitive {
    /// isize
    Isize,
    /// i64
    I64,
    /// i32
    I32,
    /// usize
    Usize,
    /// u64
    U64,
    /// u32
    U32,
    /// u8
    U8,
    /// f64
    F64,
    /// str
    Str,
    /// bool
    Bool,
}
