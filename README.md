# language

## Working
- [x] frontend
  - [x] tokenization of str
  - [x] parseing of tokenized output
  - [x] basic AST
- [ ] backend

## Parsing
`cargo run --bin=parse -- <filename>`
