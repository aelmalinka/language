#![warn(clippy::pedantic, clippy::nursery, missing_debug_implementations)]
#![deny(unsafe_code)]
use std::fs;

use clap::Parser;
use simple_logger::SimpleLogger;

#[derive(Debug, Parser)]
#[command(author, version, about)]
struct Args {
    file: String,
}

fn main() {
    SimpleLogger::new()
        .with_level(log::LevelFilter::Info)
        .env()
        .init()
        .unwrap();
    let Args { file } = Args::parse();
    println!("{:#?}", lang::parse(&fs::read_to_string(file).unwrap()));
}
