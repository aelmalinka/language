//! Parser
use std::num::TryFromIntError;

use lalrpop_util::{lalrpop_mod, ParseError};
use snafu::prelude::*;

use super::token::{Lexer, Token};
use crate::ast::Module;

/// Parse `s` into [`Module`]
/// # Errors
/// - [`Error`]
pub fn parse(s: &str) -> Result<Module, ParseError<usize, Token, Error>> {
    grammar::ModuleParser::new().parse(Lexer::new(s))
}

/// An error in parseing
#[derive(Clone, Debug, PartialEq, Eq, Snafu)]
pub enum Error {
    /// A Token caused error
    #[snafu(visibility(pub(crate)))]
    TokenError {
        #[doc(hidden)]
        source: super::token::Error,
    },
    /// Error converting to [`usize`]
    UsizeError {
        #[doc(hidden)]
        source: TryFromIntError,
    },
}

lalrpop_mod!(
    // 2023-11-21 MLT TODO: report?
    #[allow(
        clippy::pedantic,
        clippy::nursery,
        clippy::ptr_arg,
        missing_docs,
        missing_debug_implementations
    )]
    pub grammar
);

#[cfg(test)]
mod tests {
    use lalrpop_util::ParseError;

    use super::grammar::*;
    use crate::ast::*;
    use crate::frontend::token::{Lexer, Token};

    #[test]
    fn identifier() {
        assert_eq!(
            IdentifierParser::new().parse(Lexer::new("qwer")),
            Ok("qwer"),
        );
        assert_eq!(
            IdentifierParser::new().parse(Lexer::new("asdf::qwer")),
            Err(ParseError::UnrecognizedToken {
                token: (4, Token::Scope, 6),
                expected: vec![],
            })
        );
        assert_eq!(
            IdentifierParser::new().parse(Lexer::new("12")),
            Err(ParseError::UnrecognizedToken {
                token: (0, Token::LiteralInteger(12), 2),
                expected: vec!["identifier".to_string()],
            })
        );
    }

    #[test]
    fn scoped_identifier() {
        assert_eq!(
            ScopedIdentifierParser::new().parse(Lexer::new("asdf")),
            Ok(ScopedIdentifier(vec!["asdf"], true)),
        );
        assert_eq!(
            ScopedIdentifierParser::new().parse(Lexer::new("asdf::qwer")),
            Ok(ScopedIdentifier(vec!["asdf", "qwer"], true)),
        );
        assert_eq!(
            ScopedIdentifierParser::new().parse(Lexer::new("::asdf::qwer")),
            Ok(ScopedIdentifier(vec!["asdf", "qwer"], false)),
        );
    }

    #[test]
    fn r#type() {
        assert_eq!(
            TypeParser::new().parse(Lexer::new("usize")),
            Ok(Type::Primitive(Primitive::Usize)),
        );
        assert_eq!(
            TypeParser::new().parse(Lexer::new("::asdf::qwer")),
            Ok(Type::Ident(ScopedIdentifier(vec!["asdf", "qwer"], false))),
        );
        assert_eq!(
            TypeParser::new().parse(Lexer::new("&usize")),
            Ok(Type::Reference(Box::new(Type::Primitive(Primitive::Usize)))),
        );
        assert_eq!(
            TypeParser::new().parse(Lexer::new("[usize]")),
            Ok(Type::Slice(Box::new(Type::Primitive(Primitive::Usize)))),
        );
        assert_eq!(
            TypeParser::new().parse(Lexer::new("[usize; 12]")),
            Ok(Type::Array(Box::new(Type::Primitive(Primitive::Usize)), 12)),
        );
        assert_eq!(
            TypeParser::new().parse(Lexer::new("()")),
            Ok(Type::Tuple(vec![])),
        );
        assert_eq!(
            TypeParser::new().parse(Lexer::new("(asdf, bool)")),
            Ok(Type::Tuple(vec![
                Type::Ident(ScopedIdentifier(vec!["asdf"], true)),
                Type::Primitive(Primitive::Bool),
            ])),
        );
    }

    #[test]
    fn binary() {
        assert_eq!(
            ExpressionParser::new().parse(Lexer::new("11 + 22")),
            Ok(Expression::Binary(
                Box::new(Expression::Literal(Literal::Integer(11))),
                Binary::Add,
                Box::new(Expression::Literal(Literal::Integer(22))),
            )),
        );
        assert_eq!(
            ExpressionParser::new().parse(Lexer::new("33 * 11 + 22")),
            Ok(Expression::Binary(
                Box::new(Expression::Binary(
                    Box::new(Expression::Literal(Literal::Integer(33))),
                    Binary::Mul,
                    Box::new(Expression::Literal(Literal::Integer(11))),
                )),
                Binary::Add,
                Box::new(Expression::Literal(Literal::Integer(22))),
            )),
        );
        assert_eq!(
            ExpressionParser::new().parse(Lexer::new("33 * (11 + 22)")),
            Ok(Expression::Binary(
                Box::new(Expression::Literal(Literal::Integer(33))),
                Binary::Mul,
                Box::new(Expression::Binary(
                    Box::new(Expression::Literal(Literal::Integer(11))),
                    Binary::Add,
                    Box::new(Expression::Literal(Literal::Integer(22))),
                )),
            )),
        );
        assert_eq!(
            ExpressionParser::new().parse(Lexer::new("thirtythree * (11 + 22)")),
            Ok(Expression::Binary(
                Box::new(Expression::Value("thirtythree")),
                Binary::Mul,
                Box::new(Expression::Binary(
                    Box::new(Expression::Literal(Literal::Integer(11))),
                    Binary::Add,
                    Box::new(Expression::Literal(Literal::Integer(22))),
                )),
            )),
        );
    }

    #[test]
    fn empty_statement() {
        assert_eq!(
            StatementParser::new().parse(Lexer::new(";")),
            Ok(Statement::Empty),
        );
    }

    #[test]
    fn expression_statement() {
        assert_eq!(
            StatementParser::new().parse(Lexer::new("33 * (11 + 22);")),
            Ok(Statement::Expression(Expression::Binary(
                Box::new(Expression::Literal(Literal::Integer(33))),
                Binary::Mul,
                Box::new(Expression::Binary(
                    Box::new(Expression::Literal(Literal::Integer(11))),
                    Binary::Add,
                    Box::new(Expression::Literal(Literal::Integer(22))),
                )),
            ),)),
        );
    }

    #[test]
    fn binding() {
        assert_eq!(
            StatementParser::new().parse(Lexer::new(r#"let asdf = "qwer";"#)),
            Ok(Statement::Binding(
                "asdf",
                None,
                Expression::Literal(Literal::String("qwer"))
            )),
        );
        assert_eq!(
            StatementParser::new().parse(Lexer::new(r#"let asdf: &str = "qwer";"#)),
            Ok(Statement::Binding(
                "asdf",
                Some(Type::Reference(Box::new(Type::Primitive(Primitive::Str)))),
                Expression::Literal(Literal::String("qwer"))
            )),
        );
    }

    #[test]
    fn assignment() {
        assert_eq!(
            StatementParser::new().parse(Lexer::new(r#"asdf = "qwer";"#)),
            Ok(Statement::Assign(
                "asdf",
                Assignment::Eq,
                Expression::Literal(Literal::String("qwer"))
            )),
        );
        assert_eq!(
            StatementParser::new().parse(Lexer::new(r#"asdf = 33 * (11 + 22);"#)),
            Ok(Statement::Assign(
                "asdf",
                Assignment::Eq,
                Expression::Binary(
                    Box::new(Expression::Literal(Literal::Integer(33))),
                    Binary::Mul,
                    Box::new(Expression::Binary(
                        Box::new(Expression::Literal(Literal::Integer(11))),
                        Binary::Add,
                        Box::new(Expression::Literal(Literal::Integer(22))),
                    )),
                ),
            )),
        );
    }

    #[test]
    fn block() {
        assert_eq!(
            BlockParser::new().parse(Lexer::new("{}")),
            Ok(Block(vec![], None)),
        );
        assert_eq!(
            BlockParser::new().parse(Lexer::new("{ asdf }")),
            Ok(Block(vec![], Some(Expression::Value("asdf")))),
        );
    }

    #[test]
    fn r#fn() {
        assert_eq!(
            ItemParser::new().parse(Lexer::new("fn asdf() {}")),
            Ok(Item::Function {
                name: "asdf",
                params: vec![],
                r#return: None,
                block: Block(vec![], None),
            }),
        );
        assert_eq!(
            ItemParser::new().parse(Lexer::new(
                "fn asdf() {
                qwer;
            }"
            )),
            Ok(Item::Function {
                name: "asdf",
                params: vec![],
                r#return: None,
                block: Block(vec![Statement::Expression(Expression::Value("qwer"))], None),
            }),
        );
        assert_eq!(
            ItemParser::new().parse(Lexer::new(
                "fn asdf() {
                qwer
            }"
            )),
            Ok(Item::Function {
                name: "asdf",
                params: vec![],
                r#return: None,
                block: Block(vec![], Some(Expression::Value("qwer")))
            }),
        );
    }

    #[test]
    fn r#struct() {
        assert_eq!(
            ItemParser::new().parse(Lexer::new("struct asdf { zxcv: String, qwer: usize }")),
            Ok(Item::Struct {
                name: "asdf",
                fields: vec![
                    ("zxcv", Type::Ident(ScopedIdentifier(vec!["String"], true))),
                    ("qwer", Type::Primitive(Primitive::Usize)),
                ],
            }),
        );
    }

    #[test]
    fn bool() {
        assert_eq!(
            LiteralParser::new().parse(Lexer::new("true")),
            Ok(Literal::Bool(true))
        );
        assert_eq!(
            LiteralParser::new().parse(Lexer::new("false")),
            Ok(Literal::Bool(false))
        );
    }

    #[test]
    fn char() {
        assert_eq!(
            LiteralParser::new().parse(Lexer::new("'a'")),
            Ok(Literal::Char('a'))
        );

        // 2023-11-24 MLT NOTE: see below; this confirms it's not Logos I believe
        assert_eq!("💞".chars().collect::<Vec<_>>(), vec!['💞']);
        assert_eq!("'💞'".chars().nth(1), Some('💞'));

        // 2023-11-24 MLT TODO: who's choking on unicode
        /*
        assert_eq!(
            LiteralParser::new().parse(Lexer::new("'💞'")),
            Ok(Literal::Char('💞'))
        );
        */
    }

    #[test]
    fn int() {
        assert_eq!(
            LiteralParser::new().parse(Lexer::new("123")),
            Ok(Literal::Integer(123)),
        );
    }

    #[test]
    fn float() {
        assert_eq!(
            LiteralParser::new().parse(Lexer::new("42.56")),
            Ok(Literal::Float(42.56)),
        );
    }

    #[test]
    fn string() {
        assert_eq!(
            LiteralParser::new().parse(Lexer::new(r#""asdf""#)),
            Ok(Literal::String("asdf"))
        );
    }
}
